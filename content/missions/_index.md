---
title: Missions de contribution
---

Voici comment contribuer au projet :

### Organiser une rencontre entre deux collectifs

Vous connaissez des personnes dans deux collectifs qui n'ont pas eu l'occasion de se rencontrer ?

Faites-leur profiter de ce lien que vous pouvez créer, et proposez-leur une rencontre prochainement !

### Agréger des données utiles

<iframe class="airtable-embed" src="https://airtable.com/embed/shrUPE2duwxd2jo5u?backgroundColor=cyan" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>

### Proposez une mission !

<iframe class="airtable-embed" src="https://airtable.com/embed/shroSKyKJ11Ef5Pcy?backgroundColor=cyan" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>