Chèr·e·s militant·e·s de Strasbourg,

Je vous propose de nous unir pour cuisiner des repas, les livrer à vélo à nos sympathisant·e·s qui les auront commandés et dédier les sommes dégagées à nos causes.

**Réaliser cette action en réunissant des personnes de nombreux collectifs engagés pour différentes causes sera une occasion de renforcer les liens, apprendre à
être actif·ves ensemble et s'entraider.**

Nous pourrons organiser cette action pour qu'elle reflète nos valeur et nos revendications, en cuisinant des aliments locaux,
en proposant un prix libre pour permettre à toutes les bourses d'y avoir accès, et de nombreux autres choix.

Réaliser cette action une seule fois n'aura pas de grand un impact. Faisons-en progressivement une activité pérenne.

Pour que cela se lance, il faudra être nombreux·ses à contribuer. Je me mets à votre disposition pour animer des forums de contribution. Les actions qui seront nécessaires 
pour que l'activité soit lancée et se pérennise seront décrites pour que chacun·e puisse en réaliser une ou plusieurs suivant ses capacités et ses envies.

Ces missions seront rétribuées, si le montant dans la cagnotte le permet. La réciprocité qui devra toujours avoir lieu sera celle de transmettre des savoir-faire à toute
personne qui contribue.

Ces forums de contribution seront aussi l'occasion de permettre à toute personne de contribuer aux actions de nos collectifs, en particulier en réalisant de petites
missions facilitant la coopération entre les collectifs.

De nombreux·ses artisan·e·s et indépendant·e·s sont en difficulté économique suite à la pandémie. À la condition d'adhérer à la charte qui sera rédigée par les collectfs,
au cours de ces forums de contribution, il sera possible de les aider à reprendre leur activité, en particulier en recommandant leurs service autour de nous.

Par la suite, il sera possible de contribuer à faire transitionner leurs activités dans la direction des valeurs et revendications des collectifs. Les économies faites grâce
à ces transitions iront à la cagnotte qui permet la rétribution des missions de contribution réalisées.

Ces activités pourront être pérennisées dans le cadre d'une coopérative d'activité (CAE). Ce type de structure permet de mutualiser les moyens, outils et équipements,
de répartir le travail lorsqu'une activité manque de commandes, et de mettre en place un barème d'incitations ou des pénalités financières (qui évoluera dans le temps et qui
sera appliqué avec tact) pour favoriser les choix qui amènent l'activité économique à répondre aux valeur et revendications des collectifs.

Une telle CAE permettra également de donner un cadre pour le travail de personnes isolées du travail (personnes sans-abris, personnnes isolées…).

Nous nous retrouverons en Assemblée Générale inter-collectifs et en AG des travailleur·euses pour prendre les décisions collectivement.

Si cette idée vous parle, écrivez-moi s'il vous plaît un mail, même très court à [jibe.boh@orange.](mailto:jibe.boh@orange.fr)

Jibé, avec de nombreuses inspirations de personnes qui pourront se reconnaître, dont des discussions très récentes. J'ai voulu écrire ce texte, mais je ne m'en sens pas
le seul auteur·e, je vous invite à vous l'approprier, à proposer des variations dans le mécanisme, etc !

<iframe class="airtable-embed" src="https://airtable.com/embed/shrfqPANs005U1aDQ?backgroundColor=cyan" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>